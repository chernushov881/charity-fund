<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Shapely
 */

?>

</div><!-- #main -->
</section><!-- section -->

<div class="footer-callout">
	<?php shapely_footer_callout(); ?>
</div>

<footer id="colophon" class="site-footer footer bg-dark" role="contentinfo">
	<div class="container footer-inner">
		<div class="row">
			<?php get_sidebar( 'footer' ); ?>
		</div>

		<div class="row">
			<div class="site-info col-sm-12">
				<div class="copyright-text">
					<?php echo wp_kses_post( get_theme_mod( 'shapely_footer_copyright' ) ); ?>
				</div>
				<div class="footer-credits">
					<?/*php shapely_footer_info(); */?>
				</div>
			</div><!-- .site-info -->
			<div class="col-sm-6 text-right">
				<?php shapely_social_icons(); ?>
			</div>
		</div>
	</div>

	<a class="btn btn-sm fade-half back-to-top inner-link" href="#top"><i class="fa fa-angle-up"></i></a>
</footer><!-- #colophon -->
</div>
</div><!-- #page -->
<div id="modaiImg" class="modal">
<div class="modal-in">
<div class="close">X</div> 
<p>    <img src="" alt="" class="img"> 
  </p></div>
</div> 
<script>
document.addEventListener("DOMContentLoaded", function(){
    const imgs_modal = document.querySelectorAll('.img_modal')
    imgs_modal.forEach( e => {
        console.log(e.getAttribute('src'))
        let srcImg = e.getAttribute('src')
        let modal = document.querySelector('#modaiImg')
        let modalImg = modal.querySelector('.img')
        let close = modal.querySelector('.close')

        e.addEventListener('click', function(){
            modal.classList.add('open')
            modalImg.setAttribute('src', srcImg)
        })
        close.addEventListener('click', modalClose)
        modal.addEventListener('click', modalClose)
        function modalClose(e) {
            if (e.target !== modalImg) modal.classList.remove('open')
        }
    })
    function modalOpen(e) {
    	//console.log('modalOpen', e) 
        //modal.classList.add('open')
    }
})
jQuery( document ).ready( function( $ ) {
  $('.flexslider-home-our-projects').flexslider({
    animation: "slide"
  });
});
</script>
<?php wp_footer(); ?>

</body>
</html>
